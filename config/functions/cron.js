'use strict';

/**
 * Cron config that gives you an opportunity
 * to run scheduled jobs.
 *
 * The cron format consists of:
 * [MINUTE] [HOUR] [DAY OF MONTH] [MONTH OF YEAR] [DAY OF WEEK] [YEAR (optional)]
 */

module.exports = {

  /**
   * Simple example.
   * Every monday at 1am.
   */

  '1,11,21,31,41,51 * * * *': async () => {
    // rating engine cron job
    console.log('[CRON] [Rating Engine] cron job started');
    let providers = await strapi.controllers.provider.find({
      query: {}
    });
    for (let provider of providers) {
      let score = 0,
        num = 0;
      for (let order of provider.orders)
        if (order.rating) {
          score += order.rating;
          num++;
        }

      console.log(`[CRON] [Rating Engine] [provider_id] ${provider.id} [score] ${score} [num] ${num}`);

      if (num >= 1) {
        let rating = score / num;
        console.log(`[CRON] [Rating Engine] [provider_id] ${provider.id} [rating] ${rating}`);
        strapi.controllers.provider.update({
          params: {
            id: provider.id
          },
          request: {
            body: {
              rating: rating
            }
          }
        });
      }
    }
  },

  '*/10 * * * *': async () => {
    // redispatching outstanding orders
    console.log('[CRON] [Redispatching Engine] cron job started');
    const orders = await strapi.controllers.order.find({
      query: {
        status: strapi.config.currentEnvironment.ORDER_STATUS_OUTSTANDING
      }
    });

    for (let order of orders) {
      const ivrlog = await strapi.controllers.ivrlog.findOne({
        params: {
          id: order.ivrlog_id
        }
      });
      let ctx = {
        request: {
          body: ivrlog.attributes
        }
      };
      ctx.request.body.redispatch = true;
      ctx.request.body.order_id = order.id;
      console.log(`[CRON] [Redispatching Engine] [order_id] ${order.id}`);
      delete ctx.request.body.id;
      delete ctx.request.body.created_at;
      delete ctx.request.body.updated_at;
      delete ctx.request.body.createdAt;
      delete ctx.request.body.updatedAt;
      strapi.controllers.ivrlog.create(ctx)
    }
  }
};
