const axios = require('axios');

module.exports = {
  calculatePrice: async (original_price, delivery_fees, freeDeliveryEligible, discountEligible, discount_amount) => {
    const res = await axios.post('http://localhost:1338/libs/calculate-price/test', {
      original_price: original_price,
      delivery_fees: delivery_fees,
      freeDeliveryEligible: freeDeliveryEligible,
      discountEligible: discountEligible,
      discount_amount: discount_amount
    });
    return res.data;
  }
}
