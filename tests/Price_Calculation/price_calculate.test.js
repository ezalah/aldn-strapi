const functions = require('./functions');

test('free delivery 0. discount 0', async () => {
  const ret = await functions.calculatePrice(100, 10, false, false, 20);
  expect(ret).toBe(100);
})

test('free delivery 0. discount 1', async () => {
  const ret = await functions.calculatePrice(100, 10, false, true, 20);
  expect(ret).toBe(80);
})

test('free delivery 1. discount 0', async () => {
  const ret = await functions.calculatePrice(100, 10, true, false, 20);
  expect(ret).toBe(90);
})

test('free delivery 1. discount 1', async () => {
  const ret = await functions.calculatePrice(100, 10, true, true, 20);
  expect(ret).toBe(70);
})
