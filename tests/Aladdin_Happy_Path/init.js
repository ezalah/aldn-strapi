const {
  request
} = require('graphql-request');
const endpoint = 'http://dev.4aldn.com:1338/graphql';

const addCluster = async () => {
  const query = `mutation {
    createCluster (input: {
      data: {
        name: "Jasmine",
        boundaries: "[[30.99380711882234,30.05534574842851],[30.9995754440638,30.05633139370865],[31.00535229472581,30.04964132490534],[31.00364773526016,30.04857131654282],[30.99874824858447,30.04815853168476],[30.9912433565496,30.04507992993784],[30.9892665999705,30.04739977463699],[30.98812669179689,30.04822144528773]]" # [[lng,lat],[lng,lat],...]
      }
    }) {
      cluster {
        id
        name
        boundaries
      }
    }
  }`

  return request(endpoint, query);
}

const addServiceType = async (variables) => {
  const query = `mutation ($type: String, $pp: String){
    createServicetype (input: {
      data: {
        type: $type,
        profile_pic: $pp
      }
    }) {
      servicetype {
        type
        id
      }
    }
  }`

  return request(endpoint, query, variables);
}

const addProvider = async (variables) => {
  const query = `mutation ($name: String, $servicetype: ID, $cluster: ID){
    createProvider (input: {
      data: {
        name: $name,
        avg_delivery_time: 45,
        rating: 4.58,
        lat: 30.054431,
        lng: 30.994985,
        servicetype: $servicetype,
        phone_number: "+201112825132",
        #opening_time: "07:00",
        #closing_time: "20:00",
        always_available: true,	# if always available is defined, you don't have to define opening and closing times
        cluster: $cluster	# cluster id
        #parentprovider: 4,
        profile_pic: "www.google.com"
      }
    }) {
      provider {
        name
        signin_code
        id
        parentprovider {
          id
          name
        }
      }
    }
  }`

  return request(endpoint, query, variables);
}

const addProvider = async (variables) => {
  const query = `mutation {
    createDeviceglobalconf (input: {
      data: {
        Keep_Period: "096",
        Activation_Min_Click: "20",
        Service_Min_Click: "3",
        firmware_version: "v1.0.0",
        AP_Reconnect_Trials: "1",
        Time_Reset: "0"
      }
    }) {
      deviceglobalconf {
        Keep_Period
        Activation_Min_Click
        Service_Min_Click
        firmware_version
        AP_Reconnect_Trials
        Time_Reset
      }
    }
  }`

  return request(endpoint, query);
}

const createMobileGlobalConf = async () => {
  const query = `mutation {
    createMobileglobalconf (input: {
      data: {
        key: "Dummy",
        value: "Dummy"
      }
    }) {
      mobileglobalconf {
        id
        key
        value
      }
    }
  }`;
  return request(endpoint, query);

}

const work = async () => {
  console.log(await addCluster());
  console.log(await addServiceType({
    type: 'Grocery',
    pp: "www.google.com"
  }));
  console.log(await addServiceType({
    type: 'Pharmacy',
    pp: "www.google.com"
  }));
  console.log(await addServiceType({
    type: 'Taxi',
    pp: "www.google.com"
  }));
  console.log(await addServiceType({
    type: 'Veg & Fruits',
    pp: "www.google.com"
  }));
  console.log(await addServiceType({
    type: 'Supplements',
    pp: "www.google.com"
  }));
  console.log(await addServiceType({
    type: 'Laundry',
    pp: "www.google.com"
  }));
  console.log(await addServiceType({
    type: 'Maintenance',
    pp: "www.google.com"
  }));
  console.log(await addServiceType({
    type: 'Home Cleaning',
    pp: "www.google.com"
  }));
  console.log(await addServiceType({
    type: 'Car Cleaning',
    pp: "www.google.com"
  }));
  console.log(await addServiceType({
    type: 'Gas Refill',
    pp: "www.google.com"
  }));
  console.log(await addServiceType({
    type: 'Water Refill',
    pp: "www.google.com"
  }));
  console.log(await addServiceType({
    type: 'Pest',
    pp: "www.google.com"
  }));
  console.log(await addServiceType({
    type: 'Supermarket',
    pp: "www.google.com"
  }));
  console.log(await addProvider({
    name: "Fayoumy Market",
    servicetype: 1,
    cluster: 1
  }));
  console.log(await addProvider({
    name: "Helmy Pharmacy",
    servicetype: 2,
    cluster: 1
  }));
  console.log(await addDeviceGlobalConf());
  console.log(await createMobileGlobalConf());
}

work();
