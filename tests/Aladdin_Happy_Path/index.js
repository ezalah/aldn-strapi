const {
  request
} = require('graphql-request');
const endpoint = 'http://localhost:1338/graphql';

const postActivationCode = async (variables) => {
  const query = `mutation ($phoneNum: String!){
        createPhoneverification(input: {
          data: {      
            phone_number: $phoneNum
          }
        }) {
          phoneverification {
            id      
            phone_number
          }
        }
      }`

  return request(endpoint, query, variables);
}

const getActivationCode = async (variables) => {
  const query = `query ($phoneNum: String, $code: String){
        phoneverifications(sort: "created_at:desc", limit: 1, where: {
          code: $code,
          phone_number: $phoneNum,

        }) {
          created_at
          code
          id
          phone_number
        }
      }`
  return request(endpoint, query, variables);
}

const addCustomer = async (variables) => {
  const query = `mutation ($phoneNum: String, $name: String, $country_code: String) {
        createCustomer(input: {
          data: {      
            name: $name,      
            phone_number: $phoneNum
            country_code: $country_code
          }
        }) {
          customer {
            id
            name
            phone_number
            country_code
          }
        }
      }`
  return request(endpoint, query, variables);

}

const addAddress = async (variables) => {
  const query = `mutation ($lng: Float, $lat: Float, $desc: String, $BN: Int, $FN: Int, $customer: ID, $address_name: String) {
        createAddress (input: {
          data: {
            lng: $lng,
            lat: $lat,
            description: $desc,
            building_number: $BN,
            flat_number: $FN,
            customer: $customer,
            name: $address_name
          }
        }) {
          address {
            id
            created_at
            updated_at
            description
            building_number
            flat_number
            lat
            lng
            customer {
              id
              name
            }
          }
        }
      }`
  return request(endpoint, query, variables);
}

const placeOrder = async (variables) => {
  const query = `mutation ($customer: ID, $provider: ID, $voice_note: String, $notes: String, $address: ID){
        createOrder (input: {
          data: {
            voice_note: $voice_note,
            notes: $notes,
            customer: $customer,
            provider: $provider,
            channel: app,
            address: $address
          }
        }) {
          order {
            id
            notes
            voice_note
            created_at
            status
            channel
            address {
              id
              name
              description
              building_number
              flat_number
              lat
              lng
            }
            attachments {
              id
              url
            }
            customer {
              name
              id
            }
            provider {
              id
              name
              avg_delivery_time
              rating
            }
          }
        }
      }`;
  return request(endpoint, query, variables);

}

const getCustomerUsingID = async (variables) => {
  const query = `query ($customer: ID!){
        customer (id: $customer) {
          id
          created_at
          updated_at
          name
          phone_number
          profile_pic
          addresses {
            id
            lat
            lng
            building_number
            flat_number
            description
            cluster {
              id
              name
            }
          }    
        }
      }`
  return request(endpoint, query, variables);
}

const getAddressUsingID = async (variables) => {
  const query = `query ($address: ID!){
        address (id: $address) {
          id
          created_at
          updated_at
          description
          building_number
          flat_number
          lat
          lng
          cluster {
            id
            name
          }
        }
      }`;
  return request(endpoint, query, variables);
}

const listServiceTypes = async (variables) => {
  const query = `query ($cluster: ID){
        servicetypes (where: {
          clusters: $cluster
        }){
          id
          type
        }
      }`
  return request(endpoint, query, variables);
}

const getProvidersOfServiceType = async (variables) => {
  const query = `query ($servicetype: ID, $cluster: ID){
        providers (where: {    
          servicetype: $servicetype,
          cluster: $cluster
        }) {
          servicetype {
            type
            id
          }
          id
          name
          avg_delivery_time
          rating
          lat
          lng    
          profile_pic
          receiving_orders
          always_available
          opening_time
          closing_time
        }
      }`
  return request(endpoint, query, variables);
}

const myOrders = async (variables) => {
  const query = `query ($customer: ID, $deliver: String, $reject: String){
        orders (where: {
          customer: $customer,
          status_nin: [$deliver, $reject]
        }) {
          id
          created_at
          updated_at    
          status
          distance
          price
          channel
          customer {
            name
            id
          }    
          provider {
            avg_delivery_time  
            currency      
            name
            id
            servicetype {
              type
            }
          }
        }
      }`
  return request(endpoint, query, variables);
}

const screens = async () => {
  let variables = {
    phoneNum: '+201112825154', // change this for new test
    code: "1234",
    name: "TEST-Ahmed Gamal", // change this for new test
    lng: 30.993587,
    lat: 30.054149,
    desc: "Sheikh Zayed",
    BN: 1,
    FN: 1,
    address_name: "Home",
    country_code: "20"
  }
  console.log(await postActivationCode(variables));
  console.log(await getActivationCode(variables));

  let createdCustomer = await addCustomer(variables);
  console.log(createdCustomer);
  variables.customer = createdCustomer.createCustomer.customer.id;

  console.log(await addAddress(variables));

  let customer = await getCustomerUsingID(variables);
  console.log(customer);

  variables.address = customer.customer.addresses[0].id;
  let address = await getAddressUsingID(variables);
  console.log(address);

  variables.cluster = address.address.cluster.id;
  console.log(await listServiceTypes(variables));

  variables.servicetype = '2';
  let providers = await getProvidersOfServiceType(variables);
  console.log(providers);

  variables.provider = providers.providers[0].id;
  variables.voice_note = 'www.google.com';
  variables.notes = 'test note';
  let placedOrder = await placeOrder(variables);
  console.log(placedOrder);

  variables.deliver = 'Delivered';
  variables.reject = 'Rejected';
  let orders = await myOrders(variables);
  console.log(orders);

  if (orders.orders[0].id === placedOrder.createOrder.order.id)
    console.log('Exist in my order');
};

screens();
