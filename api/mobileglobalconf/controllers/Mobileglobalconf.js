'use strict';

/**
 * Mobileglobalconf.js controller
 *
 * @description: A set of functions called "actions" for managing `Mobileglobalconf`.
 */

module.exports = {

  /**
   * Retrieve mobileglobalconf records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.mobileglobalconf.search(ctx.query);
    } else {
      return strapi.services.mobileglobalconf.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a mobileglobalconf record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.mobileglobalconf.fetch(ctx.params);
  },

  /**
   * Count mobileglobalconf records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.mobileglobalconf.count(ctx.query, populate);
  },

  /**
   * Create a/an mobileglobalconf record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.mobileglobalconf.add(ctx.request.body);
  },

  /**
   * Update a/an mobileglobalconf record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.mobileglobalconf.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an mobileglobalconf record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.mobileglobalconf.remove(ctx.params);
  }
};
