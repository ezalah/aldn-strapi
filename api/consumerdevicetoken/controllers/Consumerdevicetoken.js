'use strict';

/**
 * Consumerdevicetoken.js controller
 *
 * @description: A set of functions called "actions" for managing `Consumerdevicetoken`.
 */

module.exports = {

  /**
   * Retrieve consumerdevicetoken records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.consumerdevicetoken.search(ctx.query);
    } else {
      return strapi.services.consumerdevicetoken.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a consumerdevicetoken record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.consumerdevicetoken.fetch(ctx.params);
  },

  /**
   * Count consumerdevicetoken records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.consumerdevicetoken.count(ctx.query, populate);
  },

  /**
   * Create a/an consumerdevicetoken record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.consumerdevicetoken.add(ctx.request.body);
  },

  /**
   * Update a/an consumerdevicetoken record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.consumerdevicetoken.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an consumerdevicetoken record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.consumerdevicetoken.remove(ctx.params);
  }
};
