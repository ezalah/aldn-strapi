'use strict';

/**
 * Customerhelpoption.js controller
 *
 * @description: A set of functions called "actions" for managing `Customerhelpoption`.
 */

module.exports = {

  /**
   * Retrieve customerhelpoption records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if(ctx.set) ctx.set('Content-Range', await Customerhelpoption.count());
    if (ctx.query._q) {
      return strapi.services.customerhelpoption.search(ctx.query);
    } else {
      return strapi.services.customerhelpoption.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a customerhelpoption record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.customerhelpoption.fetch(ctx.params);
  },

  /**
   * Count customerhelpoption records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.customerhelpoption.count(ctx.query, populate);
  },

  /**
   * Create a/an customerhelpoption record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.customerhelpoption.add(ctx.request.body);
  },

  /**
   * Update a/an customerhelpoption record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.customerhelpoption.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an customerhelpoption record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.customerhelpoption.remove(ctx.params);
  }
};
