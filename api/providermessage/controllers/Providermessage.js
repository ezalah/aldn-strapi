'use strict';

/**
 * Providermessage.js controller
 *
 * @description: A set of functions called "actions" for managing `Providermessage`.
 */

module.exports = {

  /**
   * Retrieve providermessage records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if(ctx.set) ctx.set('Content-Range', await Providermessage.count());
    if (ctx.query._q) {
      return strapi.services.providermessage.search(ctx.query);
    } else {
      return strapi.services.providermessage.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a providermessage record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.providermessage.fetch(ctx.params);
  },

  /**
   * Count providermessage records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.providermessage.count(ctx.query, populate);
  },

  /**
   * Create a/an providermessage record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.providermessage.add(ctx.request.body);
  },

  /**
   * Update a/an providermessage record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.providermessage.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an providermessage record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.providermessage.remove(ctx.params);
  }
};
