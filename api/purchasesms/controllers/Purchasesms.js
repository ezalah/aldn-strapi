'use strict';

/**
 * Purchasesms.js controller
 *
 * @description: A set of functions called "actions" for managing `Purchasesms`.
 */

module.exports = {

  /**
   * Retrieve purchasesms records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if(ctx.set) ctx.set('Content-Range', await Purhcasesms.count());
    if (ctx.query._q) {
      return strapi.services.purchasesms.search(ctx.query);
    } else {
      return strapi.services.purchasesms.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a purchasesms record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.purchasesms.fetch(ctx.params);
  },

  /**
   * Count purchasesms records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.purchasesms.count(ctx.query, populate);
  },

  /**
   * Create a/an purchasesms record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.purchasesms.add(ctx.request.body);
  },

  /**
   * Update a/an purchasesms record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.purchasesms.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an purchasesms record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.purchasesms.remove(ctx.params);
  },

  postCode: async (ctx) => {
    return strapi.controllers.purchasesms.create(ctx);
  }
};
