'use strict';

/**
 * Order.js controller
 *
 * @description: A set of functions called "actions" for managing `Order`.
 */

const dispatchOutstandingOrder = async (order_id, provider_id) => {
  return strapi.controllers.order.update({
    params: {
      id: order_id
    },
    request: {
      body: {
        status: strapi.config.currentEnvironment.ORDER_STATUS_NEW,
        provider: provider_id
      }
    }
  });
}

const timeAvailable = async (cand) => {
  let inRet;
  if (cand.always_available) inRet = true;
  else {
    // time-availability
    let open = cand.opening_time.split(':');
    let close = cand.closing_time.split(':');
    open = open[0] * 60 + parseInt(open[1], 10);
    close = close[0] * 60 + parseInt(close[1], 10);

    let l = Math.min(open, close);
    let r = Math.max(open, close);

    let date = new Date();
    let t = date.getHours() * 60 + date.getMinutes();

    inRet = (t >= l && t <= r);
    if (open > close) inRet = !inRet;
  }
  return inRet;
}

const getCandProviders = async (clusters, order_type) => {
  let ret = [];
  console.log(`[getCandProviders] [order_type] ${JSON.stringify(order_type)}`);
  console.log(`[getCandProviders] [order_type] ${JSON.stringify(strapi.config.currentEnvironment.IVR_ORDERTYPE_SERVICETYPE[order_type])}`);
  order_type = await strapi.controllers.servicetype.find({
    query: {
      type: strapi.config.currentEnvironment.IVR_ORDERTYPE_SERVICETYPE[order_type]
    }
  });
  console.log(`[getCandProviders] [servicetype] ${JSON.stringify(order_type)}`);
  for (let cluster of clusters) {
    let cands = await strapi.controllers.provider.find({
      query: {
        cluster: cluster, // geographical
        servicetype: order_type[0].id,
        receiving_orders: true
      }
    });
    for (let cand of cands) {
      // console.log('[getCandProviders] [cand]', cand);
      let inRet = await timeAvailable(cand);
      if (inRet) ret.push(cand);
    }
  }
  return ret;
}

const getDistance = async (x, y, x2, y2) => {
  return Math.sqrt((x2 - x) * (x2 - x) + (y2 - y) * (y2 - y));
}

const getNearestProvider = async (candProviders, lng, lat) => {
  let distance = await getDistance(candProviders[0].lng, candProviders[0].lat, lng, lat);
  let ret = candProviders[0].id;
  // console.log(candProviders);
  // console.log('--------------------------------------');
  // console.log(lng, lat);
  // console.log(candProviders[0].lng, candProviders[0].lat);
  // console.log(distance);
  for (let provider of candProviders) {
    // console.log('-----------------------------------');
    let curDist = await getDistance(provider.lng, provider.lat, lng, lat);
    // console.log(lng, lat);
    // console.log(provider.lng, provider.lat);
    // console.log(curDist, distance);
    if (curDist < distance) distance = curDist, ret = provider.id;
  }
  return ret;
}

const addOrder = async (ctx, provider, canBeOutstanding) => {
  let body = {
    request: {
      body: {
        voice_note: ctx.request.body.voice_note,
        customer: ctx.request.body.customer,
        provider: provider,
        channel: ctx.request.body.channel,
        flow_id: ctx.request.body.flow_id,
        attachments: ctx.request.body.attachments,
        notes: '',
        address: ctx.request.body.address,
        ivrlog_id: ctx.request.body.id
      }
    }
  };
  if (canBeOutstanding) body.request.body.status = strapi.config.currentEnvironment.ORDER_STATUS_OUTSTANDING;
  return strapi.controllers.order.create(body);
}

const genericDispatch = async (ctx) => {
  console.log(`[order] [genericDispatch] [body] ${ctx.request.body}`);
  let address = await strapi.controllers.address.findOne({
    params: {
      id: ctx.request.body.address
    }
  });
  if (address.attributes.cluster) {
    const clusters = [address.attributes.cluster];
    let candProviders = await getCandProviders(clusters, ctx.request.body.order_type);
    if (candProviders.length !== 0) {
      let provider = await getNearestProvider(candProviders, address.attributes.lng, address.attributes.lat);
      if (ctx.request.body.redispatch) {
        console.log(`[order] [genericDispatch] dispatching outstanding order ${ctx.request.body.order_id} to provider ${provider}`);
        return dispatchOutstandingOrder(ctx.request.body.order_id, provider);
      } else return addOrder(ctx, provider, false);
    } else {
      console.log(`[order] [genericDispatch] no cand providers`);
      if (!ctx.request.body.redispatch) {
        return addOrder(ctx, null, true)
      }
    }
  } else return {
    message: "Customer is not assigned to any cluster"
  };
}

const vendorSpecificDispatch = async (ctx) => {
  let order_type = await strapi.controllers.servicetype.find({
    query: {
      type: strapi.config.currentEnvironment.IVR_ORDERTYPE_SERVICETYPE[ctx.request.body.order_type]
    }
  });
  let device = await strapi.controllers.device.find({
    query: {
      dev_id: ctx.request.body.dev_id
    }
  });

  if (device.length !== 0) {
    let exclusiveDeviceEntry = await strapi.controllers.exclusivedevice.find({
      query: {
        device_qr: device[0].qr
      }
    });

    if (exclusiveDeviceEntry.length === 0)
      return genericDispatch(ctx);
    else {
      let entry = exclusiveDeviceEntry[0];
      let provider = await strapi.controllers.provider.findOne({
        params: {
          id: entry.provider
        }
      });
      if (provider.relations.servicetype.type !== order_type[0].type) return genericDispatch(ctx);
      console.log('[vendorSpecificDispatch] [provider]', provider)
      let ret = await timeAvailable(provider.attributes);
      if (ret) {
        if (ctx.request.body.redispatch) return dispatchOutstandingOrder(ctx.request.body.order_id, provider.attributes.id);
        else return addOrder(ctx, provider.attributes.id, false);
      } else return genericDispatch(ctx);
    }
  } else return {
    message: "No registered device with this dev_id"
  };
}

module.exports = {

  /**
   * Retrieve order records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if (ctx.set) ctx.set('Content-Range', await Order.count());
    if (ctx.query._q) {
      return strapi.services.order.search(ctx.query);
    } else {
      return strapi.services.order.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a order record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.order.fetch(ctx.params);
  },

  /**
   * Count order records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.order.count(ctx.query, populate);
  },

  /**
   * Create a/an order record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.order.add(ctx.request.body);
  },

  /**
   * Update a/an order record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.order.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an order record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.order.remove(ctx.params);
  },

  dispatch: async (ctx) => {
    console.log(`[order] [dispatch] [body] ${JSON.stringify(ctx.request.body)}`);
    if (ctx.request.body.dev_id && ctx.request.body.dev_id !== '') {
      return vendorSpecificDispatch(ctx);
    } else {
      console.log('generic dispatch');
      return genericDispatch(ctx);
    }
  },

  setOrderPrice: async (ctx) => {
    let order = await strapi.controllers.order.findOne({
      params: {
        id: ctx.request.body.order
      }
    });

    let provider = order.relations.provider.attributes;
    console.log(`[order] [setOrderPrice] [provider] ${JSON.stringify(provider)}`);

    let customer = order.relations.customer.attributes;
    console.log(`[order] [setOrderPrice] [customer] ${JSON.stringify(customer)}`);

    order = order.attributes;

    let original_price = ctx.request.body.original_price;
    console.log(`[order] [setOrderPrice] [original_price] ${original_price}`);

    let finalPrice = await strapi.controllers.lib.calculatePrice(original_price, provider.delivery_fees, order.free_delivery, order.discount, order.discount_amount);

    console.log(`[order] [setOrderPrice] [price] ${finalPrice}`);

    strapi.controllers.order.update({
      params: {
        id: order.id
      },
      request: {
        body: {
          original_price: ctx.request.body.original_price,
          price: finalPrice
        }
      }
    });

    return {
      price: finalPrice,
      discount_amount: order.discount_amount,
      original_price: ctx.request.body.original_price
    };
  }
};
