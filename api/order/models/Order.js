'use strict';

const sendFCM = require('../../../fcm/send');

const updateProviderStats = async (id) => {
  let order = await strapi.controllers.order.findOne({
    params: {
      id: id
    }
  });

  let provider = await strapi.controllers.provider.findOne({
    params: {
      id: order.attributes.provider
    }
  });

  let stats = await strapi.controllers.providerstats.findOne({
    params: {
      id: provider.attributes.providerstats
    }
  });

  return strapi.controllers.providerstats.update({
    params: {
      id: stats.attributes.id
    },
    request: {
      body: {
        delivered_orders_count: stats.attributes.delivered_orders_count + 1,
        delivered_orders_revenue: stats.attributes.delivered_orders_revenue + (order.attributes.price || 0)
      }
    }
  });
}

const updateDeliveredAt = async (id) => {
  let date = new Date();
  return strapi.controllers.order.update({
    params: {
      id: id
    },
    request: {
      body: {
        deliveredAt: date.toISOString(),
        delivered_at: date
      }
    }
  });
}

const addHistoryEntry_status = async (id, status, rejection_message) => {
  const ctx = {
    request: {
      body: {
        order: id
      }
    }
  }
  if (status === strapi.config.currentEnvironment.ORDER_STATUS_NEW) {
    ctx.request.body.type = strapi.config.currentEnvironment.ORDERHISTORY_TYPE_CREATION;
    sendNotification_provider({
      id: id,
      status: status
    });
  } else if (status === strapi.config.currentEnvironment.ORDER_STATUS_CANCELLED) {
    ctx.request.body.type = strapi.config.currentEnvironment.ORDERHISTORY_TYPE_CANCELLATION;
  } else if (status === strapi.config.currentEnvironment.ORDER_STATUS_REJECTED) {
    ctx.request.body.type = strapi.config.currentEnvironment.ORDERHISTORY_TYPE_REJECTION;
    ctx.request.body.rejection_message = rejection_message;
  } else {
    ctx.request.body.type = strapi.config.currentEnvironment.ORDERHISTORY_TYPE_STATUSCHANGE;
    ctx.request.body.status = status;
    if (status === strapi.config.currentEnvironment.ORDER_STATUS_DELIVERED) updateProviderStats(id), updateDeliveredAt(id);
  }
  strapi.controllers.orderhistory.create(ctx);
}

const addCustomerOfferData = async (model, attrs) => {
  const order = await strapi.controllers.order.findOne({
    params: {
      id: attrs[0]
    }
  });
  const customer = order.relations.customer.attributes;
  let customerOffer = await strapi.controllers.customeroffer.getCustomerOffer('Default', order.attributes.created_at);

  if (!customerOffer.applies)
    customerOffer = await strapi.controllers.customeroffer.getCustomerOffer(customer.id.toString(), order.attributes.created_at);
  delete customerOffer.applies;
  strapi.controllers.order.update({
    params: {
      id: attrs[0]
    },
    request: {
      body: customerOffer
    }
  })
}

const updateCreatedAt = async (model, attrs) => {
  let date = new Date(model.attributes.created_at);
  return strapi.controllers.order.update({
    params: {
      id: attrs[0]
    },
    request: {
      body: {
        createdAt: date.toISOString(),
        updatedAt: date.toISOString()
      }
    }
  });
}

const sendNotification_customer = async (params) => {
  console.log(`[order_model] [sendNotification_customer] [params] ${JSON.stringify(params)}`);
  const order = await strapi.controllers.order.findOne({
    params: {
      id: params.id
    }
  });
  console.log(`[order_model] [sendNotification_customer] [order] ${JSON.stringify(order)}`);
  console.log(`[order_model] [sendNotification_customer] [customer] ${JSON.stringify(order.relations.customer)}`);

  let tokens = await strapi.controllers.consumerdevicetoken.find({
    query: {
      customer_id: order.relations.customer.id
    }
  });
  console.log(`[order_model] [sendNotification_customer] [token] ${JSON.stringify(tokens)}`);

  for (let token of tokens) {
    if (strapi.config.currentEnvironment.NOTIFICATION_BODY[token.language]['status_update'][params.status]) {
      let message = {
        data: {
          screenName: strapi.config.currentEnvironment.NOTIFICATION_SCREENNAME[params.status],
          orderID: params.id
        },
        notification: {
          title: `${strapi.config.currentEnvironment.NOTIFICATION_TITLE[token.language]['status_update']}${params.id}`,
          body: strapi.config.currentEnvironment.NOTIFICATION_BODY[token.language]['status_update'][params.status]
        }
      }
      sendFCM.sendNotification([token.token], message);
    }
  }
}

const sendNotification_provider = async (params) => {
  console.log(`[order_model] [sendNotification_provider] [params] ${JSON.stringify(params)}`);
  const order = await strapi.controllers.order.findOne({
    params: {
      id: params.id
    }
  });
  console.log(`[order_model] [sendNotification_provider] [order] ${JSON.stringify(order)}`);
  console.log(`[order_model] [sendNotification_provider] [provider] ${JSON.stringify(order.relations.provider)}`);

  let tokens = await strapi.controllers.providerdevicetoken.find({
    query: {
      provider_id: order.relations.provider.id.toString()
    }
  });
  console.log(`[order_model] [sendNotification_provider] [token] ${JSON.stringify(tokens)}`);

  for (let token of tokens) {
    if (strapi.config.currentEnvironment.NOTIFICATION_BODY[token.language]['status_update'][params.status]) {
      let message = {
        data: {
          sound: strapi.config.currentEnvironment.PROVIDER_NOTIFICATION_DATA_SOUND,
          orderID: params.id.toString(),
          title: `${strapi.config.currentEnvironment.NOTIFICATION_TITLE[token.language]['order_submission']}${params.id}`,
          body: strapi.config.currentEnvironment.NOTIFICATION_BODY[token.language]['status_update'][params.status]
        }
      }
      sendFCM.sendNotification([token.token], message);
    }
  }
}

/**
 * Lifecycle callbacks for the `Order` model.
 */

module.exports = {
  // Before saving a value.
  // Fired before an `insert` or `update` query.
  // beforeSave: async (model, attrs, options) => {},

  // After saving a value.
  // Fired after an `insert` or `update` query.
  // afterSave: async (model, response, options) => {},

  // Before fetching a value.
  // Fired before a `fetch` operation.
  // beforeFetch: async (model, columns, options) => {},

  // After fetching a value.
  // Fired after a `fetch` operation.
  // afterFetch: async (model, response, options) => {},

  // Before fetching all values.
  // Fired before a `fetchAll` operation.
  // beforeFetchAll: async (model, columns, options) => {},

  // After fetching all values.
  // Fired after a `fetchAll` operation.
  // afterFetchAll: async (model, response, options) => {},

  // Before creating a value.
  // Fired before an `insert` query.
  // beforeCreate: async (model, attrs, options) => {},

  // After creating a value.
  // Fired after an `insert` query.
  afterCreate: async (model, attrs, options) => {
    addHistoryEntry_status(model.attributes.id, model.attributes.status);
    setTimeout(() => {
      addCustomerOfferData(model, attrs);
      if (model.attributes.status === strapi.config.currentEnvironment.ORDER_STATUS_NEW)
        sendNotification_provider(model.attributes);
    }, 100);
    console.log(await updateCreatedAt(model, attrs));

  },

  // Before updating a value.
  // Fired before an `update` query.
  beforeUpdate: async (model, attrs, options) => {
    // console.log(model);
    // console.log(attrs);
    if (attrs.status)
      addHistoryEntry_status(model.attributes.id, attrs.status, attrs.rejection_message),
      sendNotification_customer(model.attributes);
  },

  // After updating a value.
  // Fired after an `update` query.
  // afterUpdate: async (model, attrs, options) => {},

  // Before destroying a value.
  // Fired before a `delete` query.
  // beforeDestroy: async (model, attrs, options) => {},

  // After destroying a value.
  // Fired after a `delete` query.
  // afterDestroy: async (model, attrs, options) => {}
};
