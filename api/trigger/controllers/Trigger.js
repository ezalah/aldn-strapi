'use strict';

var querystring = require('querystring');
var request = require('async-request');
const uuid = require('uuid');

const sendToTwilio = async (customer, address, channel, cluster, gender, dev_id, ctx) => {
  const flow_id = uuid.v1();
  console.log('[sendToTwilio]', '[flow_id]', flow_id);
  console.log('[sendToTwilio]', '[customer]', JSON.stringify(customer));
  console.log('[sendToTwilio]', '[channel]', channel);
  var form = {
    "To": `+${customer.country_code}${customer.phone_number}`,
    "From": "+12018624557",
    "Parameters": JSON.stringify({
      "dev_id": dev_id,
      "flow_id": flow_id,
      "address": address,
      "channel": channel,
      "base_url": strapi.config.currentEnvironment.SERVER_BASE_URL,
      "cluster": cluster,
      "gender": gender,
      "customer_id": customer.id
    })
  };
  console.log('[sendToTwilio]', '[form]', JSON.stringify(form));
  var formData = querystring.stringify(form);
  var contentLength = formData.length;

  let resp = await request('https://AC56f2405c78c50fc35377f6aa960adf05:8d0e7302275552b7db5608633e767b79@studio.twilio.com/v1/Flows/FWf8ccfd34dd80c3bf6aaecc164f86cca1/Executions', {
    headers: {
      'Content-Length': contentLength,
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: formData,
    method: 'POST'
  });

  if (resp.statusCode !== 200) {
    let body = JSON.parse(resp.body);
    return {
      message: body.message
    };
  } else {
    if (ctx) {
      strapi.controllers.trigger.create({
        request: {
          body: {
            dev_id: ctx.request.body.dev_id,
            flow_id: flow_id
          }
        }
      });
    }
    return {
      message: 'Phone call sent'
    }
  }
}

/**
 * Trigger.js controller
 *
 * @description: A set of functions called "actions" for managing `Trigger`.
 */

module.exports = {

  /**
   * Retrieve trigger records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if (ctx.set) ctx.set('Content-Range', await Trigger.count());
    if (ctx.query._q) {
      return strapi.services.trigger.search(ctx.query);
    } else {
      return strapi.services.trigger.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a trigger record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.trigger.fetch(ctx.params);
  },

  /**
   * Count trigger records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.trigger.count(ctx.query, populate);
  },

  /**
   * Create a/an trigger record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.trigger.add(ctx.request.body);
  },

  /**
   * Update a/an trigger record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.trigger.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an trigger record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.trigger.remove(ctx.params);
  },

  callPhone_Hard: async (ctx) => {
    console.log('[callPhone_Hard]', '[body]', JSON.stringify(ctx.request.body));
    let dev = await strapi.controllers.device.find({
      query: {
        dev_id: ctx.request.body.dev_id
      }
    });
    if (dev.length === 0) {
      return {
        message: 'Device not registered'
      };
    } else {
      dev = dev[0];
      console.log('[callPhone_Hard]', '[dev]', JSON.stringify(dev));
      if (dev.suspended) return {
        message: "Device is currently suspended due to system abuse"
      };
      else {
        let customer = await strapi.controllers.customer.findOne({
          params: {
            id: dev.customer.id
          }
        });
        if (customer.length === 0) {
          return {
            message: "Customer not found. Perhaps the entry was manually deleted from Admin CLI?"
          };
        } else {
          console.log('[callPhone_Hard]', '[customer]', JSON.stringify(customer.attributes));

          // dev = dev.models[0].attributes
          console.log('[callPhone_Hard]', '[dev_id]', ctx.request.body.dev_id);
          let address = await strapi.controllers.address.find({
            query: {
              customer: customer.attributes.id,
              default: true,
              active: true
            }
          });
          if (address.length === 0) {
            return {
              message: "No default address assigned to this customer"
            };
          } else {
            console.log(`[callPhone_Hard] [address] ${JSON.stringify(address)}`);
            let ret = await sendToTwilio(customer.attributes, address[0].id, 'button', address[0].cluster.name, customer.attributes.gender, ctx.request.body.dev_id, ctx);
            console.log('[callPhone_Hard]', '[response]', JSON.stringify(ret));
            return ret;
          }
        }
      }
    }
  },

  callPhone_Soft: async (ctx) => {
    console.log('[callPhone_Soft]', '[body]', JSON.stringify(ctx.request.body));

    let address = await strapi.controllers.address.find({
      query: {
        customer: ctx.request.body.id,
        default: true,
        active: true
      }
    });

    console.log('[callPhone_Soft]', '[address]', JSON.stringify(address));
    address = address[0];
    let customer = await strapi.controllers.customer.findOne({
      params: {
        id: ctx.request.body.id
      }
    });
    console.log('[callPhone_Soft]', '[customer]', JSON.stringify(customer.attributes));

    let cluster = address.cluster;
    let ret = await sendToTwilio(customer.attributes, address.id, 'soft', cluster.name, customer.attributes.gender);
    console.log('[callPhone_Soft]', '[response]', JSON.stringify(ret));
    return ret;
  }
};
