'use strict';

/**
 * Provider.js controller
 *
 * @description: A set of functions called "actions" for managing `Provider`.
 */

module.exports = {

  /**
   * Retrieve provider records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if (ctx.set) ctx.set('Content-Range', await Provider.count());
    if (ctx.query._q) {
      return strapi.services.provider.search(ctx.query);
    } else {
      return strapi.services.provider.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a provider record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.provider.fetch(ctx.params);
  },

  /**
   * Count provider records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.provider.count(ctx.query, populate);
  },

  /**
   * Create a/an provider record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.provider.add(ctx.request.body);
  },

  /**
   * Update a/an provider record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.provider.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an provider record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.provider.remove(ctx.params);
  },

  favoriteProviders: async (ctx, next) => {
    const address = await strapi.controllers.address.findOne({
      params: {
        id: ctx.request.body.address
      }
    });
    console.log(strapi.config.currentEnvironment.LINEBREAK);
    if (!address) return {
      errorMessage: "Invalid address"
    }
    console.log(`[favoriteProviders] [address] ${JSON.stringify(address)}`);
    console.log(strapi.config.currentEnvironment.LINEBREAK);
    console.log(`[favoriteProviders] [cluster] ${JSON.stringify(address.relations.cluster)}`);
    console.log(strapi.config.currentEnvironment.LINEBREAK);
    let providers = await strapi.controllers.provider.find({
      query: {
        cluster: address.relations.cluster.id
      }
    })
    console.log(`[favoriteProviders] [providers] ${JSON.stringify(providers)}`);
    console.log(strapi.config.currentEnvironment.LINEBREAK);

    let providerIds = providers.map(({
      id
    }) => id);

    let favoriteEntries = await strapi.controllers.favoriteprovider.find({
      query: {
        customer: ctx.request.body.customer,
        provider_in: providerIds
      }
    });

    let favoriteIds = favoriteEntries.map(obj => obj.provider.id);
    let favorite = [];
    for (let obj of favoriteEntries) {
      let curProvider = await strapi.controllers.provider.findOne({
        params: {
          id: obj.provider.id
        }
      });

      favorite.push({
        favorite_id: obj.id,
        id: obj.provider.id,
        name: obj.provider.name,
        servicetype: curProvider.relations.servicetype.id,
        avg_delivery_time: obj.provider.avg_delivery_time,
        rating: obj.provider.rating,
        lat: obj.provider.lat,
        lng: obj.provider.lng,
        created_at: obj.provider.created_at,
        updated_at: obj.provider.updated_at,
        signin_code: obj.provider.signin_code,
        phone_number: obj.provider.phone_number,
        receiving_orders: obj.provider.receiving_orders,
        always_available: obj.provider.always_available,
        opening_time: obj.provider.opening_time,
        closing_time: obj.provider.closing_time,
        cluster: obj.provider.cluster.id,
        currency: obj.provider.currency,
        providerstats: (obj.provider.providerstats || {
          id: null
        }).id,
        parentprovider: (obj.provider.parentprovider || {
          id: null
        }).id,
        delivery_fees: obj.provider.delivery_fees,
        website: obj.provider.website,
        contact_person: obj.provider.contact_person,
        manager_primary_phone: obj.provider.manager_primary_phone,
        manager_secondary_phone: obj.provider.manager_secondary_phone,
        url: obj.provider.url,
        profile_pic: obj.provider.profile_pic,
        favorite_provider: obj.provider.favorite_provider
      });
    }
    console.log(`[favoriteProviders] [favoriteEntries] ${JSON.stringify(favoriteEntries)}`);
    console.log(strapi.config.currentEnvironment.LINEBREAK);

    console.log(`[favoriteProviders] [favorite] ${JSON.stringify(favorite)}`);
    console.log(strapi.config.currentEnvironment.LINEBREAK);

    let nonfavorite = [];
    for (let obj of providers) {
      if (!favoriteIds.includes(obj.id)) nonfavorite.push({
        id: obj.id,
        name: obj.name,
        servicetype: obj.servicetype.id,
        avg_delivery_time: obj.avg_delivery_time,
        rating: obj.rating,
        lat: obj.lat,
        lng: obj.lng,
        created_at: obj.created_at,
        updated_at: obj.updated_at,
        signin_code: obj.signin_code,
        phone_number: obj.phone_number,
        receiving_orders: obj.receiving_orders,
        always_available: obj.always_available,
        opening_time: obj.opening_time,
        closing_time: obj.closing_time,
        cluster: obj.cluster.id,
        currency: obj.currency,
        providerstats: (obj.providerstats || {
          id: null
        }).id,
        parentprovider: (obj.parentprovider || {
          id: null
        }).id,
        delivery_fees: obj.delivery_fees,
        website: obj.website,
        contact_person: obj.contact_person,
        manager_primary_phone: obj.manager_primary_phone,
        manager_secondary_phone: obj.manager_secondary_phone,
        url: obj.url,
        profile_pic: obj.profile_pic,
        favorite_provider: obj.favorite_provider
      });
    }
    console.log(`[favoriteProviders] [nonfavorite] ${JSON.stringify(nonfavorite)}`);
    console.log(strapi.config.currentEnvironment.LINEBREAK);
    return {
      favorite: favorite,
      nonfavorite: nonfavorite
    }
  }
};
