'use strict';

/**
 * Customerhelpoptionchild.js controller
 *
 * @description: A set of functions called "actions" for managing `Customerhelpoptionchild`.
 */

module.exports = {

  /**
   * Retrieve customerhelpoptionchild records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if(ctx.set) ctx.set('Content-Range', await Customerhelpoptionchild.count());
    if (ctx.query._q) {
      return strapi.services.customerhelpoptionchild.search(ctx.query);
    } else {
      return strapi.services.customerhelpoptionchild.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a customerhelpoptionchild record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.customerhelpoptionchild.fetch(ctx.params);
  },

  /**
   * Count customerhelpoptionchild records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.customerhelpoptionchild.count(ctx.query, populate);
  },

  /**
   * Create a/an customerhelpoptionchild record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.customerhelpoptionchild.add(ctx.request.body);
  },

  /**
   * Update a/an customerhelpoptionchild record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.customerhelpoptionchild.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an customerhelpoptionchild record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.customerhelpoptionchild.remove(ctx.params);
  }
};
