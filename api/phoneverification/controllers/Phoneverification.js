'use strict';

/**
 * Phoneverification.js controller
 *
 * @description: A set of functions called "actions" for managing `Phoneverification`.
 */

module.exports = {

  /**
   * Retrieve phoneverification records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if(ctx.set) ctx.set('Content-Range', await Phoneverification.count());
    if (ctx.query._q) {
      return strapi.services.phoneverification.search(ctx.query);
    } else {
      return strapi.services.phoneverification.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a phoneverification record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.phoneverification.fetch(ctx.params);
  },

  /**
   * Count phoneverification records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.phoneverification.count(ctx.query, populate);
  },

  /**
   * Create a/an phoneverification record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.phoneverification.add(ctx.request.body);
  },

  /**
   * Update a/an phoneverification record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.phoneverification.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an phoneverification record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.phoneverification.remove(ctx.params);
  }
};
