'use strict';

const rand = require('randomatic');

/**
 * Lifecycle callbacks for the `Phoneverification` model.
 */

module.exports = {
  // Before saving a value.
  // Fired before an `insert` or `update` query.
  // beforeSave: async (model, attrs, options) => {},

  // After saving a value.
  // Fired after an `insert` or `update` query.
  // afterSave: async (model, response, options) => {},

  // Before fetching a value.
  // Fired before a `fetch` operation.
  // beforeFetch: async (model, columns, options) => {},

  // After fetching a value.
  // Fired after a `fetch` operation.
  // afterFetch: async (model, response, options) => {},

  // Before fetching all values.
  // Fired before a `fetchAll` operation.
  // beforeFetchAll: async (model, columns, options) => {},

  // After fetching all values.
  // Fired after a `fetchAll` operation.
  // afterFetchAll: async (model, response, options) => {},

  // Before creating a value.
  // Fired before an `insert` query.
  // beforeCreate: async (model, attrs, options) => {},

  // After creating a value.
  // Fired after an `insert` query.
  afterCreate: async (model, attrs, options) => {
    const id = attrs[0];
    const env = strapi.config.currentEnvironment;
    try {

      if (strapi.config.environment === 'development') {
        const activationCode = "1234";

        const ret = await strapi.controllers.phoneverification.update({
          params: {
            id: id
          },
          request: {
            body: {
              code: activationCode
            }
          }
        });

      }
      // sending the sms
      else {
        const activationCode = rand('0', 4);

        const res = await strapi.controllers.util.SMSMisr_send_SMS(env.SMSMISR_USERNAME, env.SMSMISR_PASSWORD, env.SMSMISR_LANGUAGE_EN, env.SMSMISR_SENDER_ALADDIN, model.attributes.phone_number, `Your code is: ${activationCode}`);
        console.log(`[phoneverification] [afterCreate] [SMSMisr_send_SMS] [res] ${JSON.stringify(res)}`);
        if (res.code === "1901") {
          console.log('rescode 1901')
          strapi.controllers.phoneverification.update({
            params: {
              id: id
            },
            request: {
              body: {
                code: activationCode
              }
            }
          });
        }
      }
    } catch (error) {
      console.error(error);
    }
  },

  // Before updating a value.
  // Fired before an `update` query.
  // beforeUpdate: async (model, attrs, options) => {},

  // After updating a value.
  // Fired after an `update` query.
  // afterUpdate: async (model, attrs, options) => {
  //   timeUpdate.updatedAtTime(strapi.controllers.phoneverification.update, attrs[0]);
  // },

  // Before destroying a value.
  // Fired before a `delete` query.
  // beforeDestroy: async (model, attrs, options) => {},

  // After destroying a value.
  // Fired after a `delete` query.
  // afterDestroy: async (model, attrs, options) => {}
};
