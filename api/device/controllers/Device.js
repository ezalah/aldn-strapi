'use strict';

/**
 * Device.js controller
 *
 * @description: A set of functions called "actions" for managing `Device`.
 */

module.exports = {

  /**
   * Retrieve device records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if (ctx.set) ctx.set('Content-Range', await Device.count());
    if (ctx.query._q) {
      return strapi.services.device.search(ctx.query);
    } else {
      return strapi.services.device.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a device record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.device.fetch(ctx.params);
  },

  /**
   * Count device records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.device.count(ctx.query, populate);
  },

  /**
   * Create a/an device record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.device.add(ctx.request.body);
  },

  /**
   * Update a/an device record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.device.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an device record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.device.remove(ctx.params);
  },

  deviceIntegration: async (ctx) => {
    console.log('[deviceIntegration]', '[body]', JSON.stringify(ctx.request.body));
    let existingDev = await strapi.controllers.device.find({
      query: {
        dev_id: ctx.request.body.dev_id
      }
    });
    if (existingDev.length === 0) {
      existingDev = await strapi.controllers.device.find({
        query: {
          qr: ctx.request.body.qr
        }
      });
      if (existingDev.length === 0) {
        ctx.response.status = strapi.config.currentEnvironment.DEVICE_DEVICEINTEGRATION_QRNOTEXIST_CODE;
        return {
          message: "This qr code is not associated with any device in the system"
        };
      } else if (!existingDev[0].dev_id && !existingDev[0].customer) {
        let ret = await strapi.controllers.device.update({
          params: {
            id: existingDev[0].id
          },
          request: {
            body: {
              dev_id: ctx.request.body.dev_id,
              customer: ctx.request.body.customer_id,
            }
          }
        });
        console.log('[deviceIntegration]', '[response]', JSON.stringify(ret));
        return ret;
      } else {
        ctx.response.status = strapi.config.currentEnvironment.DEVICE_DEVICEINTEGRATION_DEVICEALREADYREG_CODE;
        return {
          message: "This device has already been registered with some customer and device id"
        };
      }
    } else {
      ctx.response.status = strapi.config.currentEnvironment.DEVICE_DEVICEINTEGRATION_DEVIDEXIST_CODE;
      let ret = {
        message: "This device id already exists with some registered device"
      };
      console.log('[deviceIntegration]', '[response]', JSON.stringify(ret));
      return ret;
    }
  }
};
