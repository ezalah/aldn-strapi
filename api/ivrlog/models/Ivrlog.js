'use strict';

/**
 * Lifecycle callbacks for the `Ivrlog` model.
 */

module.exports = {
  // Before saving a value.
  // Fired before an `insert` or `update` query.
  // beforeSave: async (model, attrs, options) => {},

  // After saving a value.
  // Fired after an `insert` or `update` query.
  // afterSave: async (model, response, options) => {},

  // Before fetching a value.
  // Fired before a `fetch` operation.
  // beforeFetch: async (model, columns, options) => {},

  // After fetching a value.
  // Fired after a `fetch` operation.
  // afterFetch: async (model, response, options) => {},

  // Before fetching all values.
  // Fired before a `fetchAll` operation.
  // beforeFetchAll: async (model, columns, options) => {},

  // After fetching all values.
  // Fired after a `fetchAll` operation.
  // afterFetchAll: async (model, response, options) => {},

  // Before creating a value.
  // Fired before an `insert` query.
  // beforeCreate: async (model, attrs, options) => {},

  // After creating a value.
  // Fired after an `insert` query.
  afterCreate: async (model, attrs, options) => {
    console.log('[Ivrlog_afterCreate] [model]', JSON.stringify(model.attributes));
    let customer = await strapi.controllers.customer.findOne({
      params: {
        id: model.attributes.customer_id
      },
      query: {
        country_code: model.attributes.country_code,
        phone_number: model.attributes.phone_number
      }
    });
    console.log('[Ivrlog_afterCreate] [customer]', JSON.stringify(customer));
    if (customer.length !== 0) {

      model.attributes.customer = customer.attributes.id;
      const order = await strapi.controllers.order.dispatch({
        request: {
          body: model.attributes
        }
      });
      console.log('[Ivrlog_afterCreate] [ret]', JSON.stringify(order));
    } else
      console.log('[Ivrlog_afterCreate] [ret]', 'No customer with this number');
  },

  // Before updating a value.
  // Fired before an `update` query.
  // beforeUpdate: async (model, attrs, options) => {},

  // After updating a value.
  // Fired after an `update` query.
  // afterUpdate: async (model, attrs, options) => {},

  // Before destroying a value.
  // Fired before a `delete` query.
  // beforeDestroy: async (model, attrs, options) => {},

  // After destroying a value.
  // Fired after a `delete` query.
  // afterDestroy: async (model, attrs, options) => {}
};
