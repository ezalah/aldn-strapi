'use strict';

/**
 * Ivrlog.js controller
 *
 * @description: A set of functions called "actions" for managing `Ivrlog`.
 */

module.exports = {

  /**
   * Retrieve ivrlog records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if (ctx.set) ctx.set('Content-Range', await Ivrlog.count());
    if (ctx.query._q) {
      return strapi.services.ivrlog.search(ctx.query);
    } else {
      return strapi.services.ivrlog.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a ivrlog record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.ivrlog.fetch(ctx.params);
  },

  /**
   * Count ivrlog records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.ivrlog.count(ctx.query, populate);
  },

  /**
   * Create a/an ivrlog record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.ivrlog.add(ctx.request.body);
  },

  /**
   * Update a/an ivrlog record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.ivrlog.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an ivrlog record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.ivrlog.remove(ctx.params);
  },

  logEntry: async (ctx) => {
    return strapi.controllers.ivrlog.create({
      request: {
        body: {
          order_type: strapi.config.currentEnvironment.IVR_ORDERTYPE[ctx.request.body.order_type],
          voice_note: ctx.request.body.voice_note || '',
          whatsapp: ctx.request.body.whatsapp,
          country_code: ctx.request.body.country_code,
          phone_number: ctx.request.body.phone_number,
          flow_id: ctx.request.body.flow_id,
          dev_id: ctx.request.body.dev_id,
          address: ctx.request.body.address,
          channel: ctx.request.body.channel,
          customer_id: ctx.request.body.customer_id
        }
      }
    })
  },

  postCall: async (ctx) => {
    console.log('[postCall] [body]', ctx.request.body);
    let customer = await strapi.controllers.customer.find({
      query: {
        country_code: ctx.request.body.country_code,
        phone_number: ctx.request.body.phone_number
      }
    });
    if (customer.length !== 0) {
      ctx.request.body.customer = customer[0].id;
      const order = await strapi.controllers.order.dispatch(ctx);
      ctx.request.body.order = order.id;
      strapi.controllers.ivrlog.logEntry(ctx);
      return {
        message: "Post call sequence initiated"
      }
    } else return {
      message: "No customer with this number"
    };
  },

  whatsappMedia: async (ctx) => {
    // console.log(ctx.request.body);
    const attachment = await strapi.controllers.attachment.create({
      request: {
        body: {
          url: ctx.request.body.MediaUrl0
        }
      }
    });
    let customer = await strapi.controllers.customer.find({
      query: {
        country_code: ctx.request.body.country_code,
        phone_number: ctx.request.body.phone_number
      }
    });
    if (customer.length !== 0) {
      ctx.request.body.customer = customer[0].id;
      ctx.request.body.attachments = [attachment.id];
      const order = await strapi.controllers.order.dispatch(ctx);
      return order;
    } else return {
      message: "No customer with this number"
    }
  }
};
