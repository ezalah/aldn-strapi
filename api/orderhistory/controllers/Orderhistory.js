'use strict';

/**
 * Orderhistory.js controller
 *
 * @description: A set of functions called "actions" for managing `Orderhistory`.
 */

module.exports = {

  /**
   * Retrieve orderhistory records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if(ctx.set) ctx.set('Content-Range', await Orderhistory.count());
    if (ctx.query._q) {
      return strapi.services.orderhistory.search(ctx.query);
    } else {
      return strapi.services.orderhistory.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a orderhistory record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.orderhistory.fetch(ctx.params);
  },

  /**
   * Count orderhistory records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.orderhistory.count(ctx.query, populate);
  },

  /**
   * Create a/an orderhistory record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.orderhistory.add(ctx.request.body);
  },

  /**
   * Update a/an orderhistory record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.orderhistory.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an orderhistory record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.orderhistory.remove(ctx.params);
  }
};
