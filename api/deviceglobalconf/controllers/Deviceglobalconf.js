'use strict';

/**
 * Deviceglobalconf.js controller
 *
 * @description: A set of functions called "actions" for managing `Deviceglobalconf`.
 */

module.exports = {

  /**
   * Retrieve deviceglobalconf records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if(ctx.set) ctx.set('Content-Range', await Deviceglobalconf.count());
    if (ctx.query._q) {
      return strapi.services.deviceglobalconf.search(ctx.query);
    } else {
      return strapi.services.deviceglobalconf.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a deviceglobalconf record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.deviceglobalconf.fetch(ctx.params);
  },

  /**
   * Count deviceglobalconf records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.deviceglobalconf.count(ctx.query, populate);
  },

  /**
   * Create a/an deviceglobalconf record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.deviceglobalconf.add(ctx.request.body);
  },

  /**
   * Update a/an deviceglobalconf record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.deviceglobalconf.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an deviceglobalconf record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.deviceglobalconf.remove(ctx.params);
  }
};
