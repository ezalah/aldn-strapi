'use strict';

/**
 * Util.js controller
 *
 * @description: A set of functions called "actions" for managing `Util`.
 */
const axios = require('axios');


module.exports = {

  /**
   * Retrieve util records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if (ctx.query._q) {
      return strapi.services.util.search(ctx.query);
    } else {
      return strapi.services.util.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a util record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.util.fetch(ctx.params);
  },

  /**
   * Count util records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.util.count(ctx.query, populate);
  },

  /**
   * Create a/an util record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.util.add(ctx.request.body);
  },

  /**
   * Update a/an util record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.util.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an util record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.util.remove(ctx.params);
  },

  SMSMisr_send_SMS: async (username, password, lang, sender, phone, msg) => {
    const env = strapi.config.currentEnvironment;
    let url = `${env.SMSMISR_API_URL}username=${username}&password=${password}&language=${lang}&sender=${sender}&mobile=${phone}&message=${msg}`;
    const res = await axios.post(url);
    return res.data;
  }
};
