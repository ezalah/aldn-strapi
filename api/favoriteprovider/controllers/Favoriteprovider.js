'use strict';

/**
 * Favoriteprovider.js controller
 *
 * @description: A set of functions called "actions" for managing `Favoriteprovider`.
 */

module.exports = {

  /**
   * Retrieve favoriteprovider records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if(ctx.set) ctx.set('Content-Range', await Favoriteprovider.count());
    if (ctx.query._q) {
      return strapi.services.favoriteprovider.search(ctx.query);
    } else {
      return strapi.services.favoriteprovider.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a favoriteprovider record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.favoriteprovider.fetch(ctx.params);
  },

  /**
   * Count favoriteprovider records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.favoriteprovider.count(ctx.query, populate);
  },

  /**
   * Create a/an favoriteprovider record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.favoriteprovider.add(ctx.request.body);
  },

  /**
   * Update a/an favoriteprovider record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.favoriteprovider.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an favoriteprovider record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.favoriteprovider.remove(ctx.params);
  }
};
