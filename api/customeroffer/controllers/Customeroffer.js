'use strict';

/**
 * Customeroffer.js controller
 *
 * @description: A set of functions called "actions" for managing `Customeroffer`.
 */

const offerEligible = async (type, customeroffer, order_created_at) => {
  return customeroffer[`${type}_flag`] === true && customeroffer[`consumed_${type}`] < customeroffer[`${type}_times`] &&
    order_created_at >= customeroffer[`${type}_start_time`] && order_created_at <= customeroffer[`${type}_end_time`];
}

module.exports = {

  /**
   * Retrieve customeroffer records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if (ctx.query._q) {
      return strapi.services.customeroffer.search(ctx.query);
    } else {
      return strapi.services.customeroffer.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a customeroffer record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.customeroffer.fetch(ctx.params);
  },

  /**
   * Count customeroffer records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.customeroffer.count(ctx.query, populate);
  },

  /**
   * Create a/an customeroffer record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.customeroffer.add(ctx.request.body);
  },

  /**
   * Update a/an customeroffer record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.customeroffer.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an customeroffer record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.customeroffer.remove(ctx.params);
  },

  getCustomerOffer: async (id, order_created_at) => {
    let customeroffer = await strapi.controllers.customeroffer.find({
      query: {
        customer_id: id
      }
    });

    if (customeroffer.length === 0) return {
      applies: false,
      free_delivery: false,
      discount: false,
      discount_amount: 0
    };
    else {
      let consumed_free_delivery = customeroffer[0].consumed_free_delivery,
        consumed_discount = customeroffer[0].consumed_discount;
      console.log(`[customeroffer] [getCustomerOffer] [customeroffer] ${JSON.stringify(customeroffer[0])}`);

      let freeDeliveryEligible = await offerEligible('free_delivery', customeroffer[0], order_created_at);
      let discountEligible = await offerEligible('discount', customeroffer[0], order_created_at);

      if (freeDeliveryEligible) {
        consumed_free_delivery++;
        console.log(`[customeroffer] [getCustomerOffer] [note] free delivery eligible`);
        console.log(`[customeroffer] [getCustomerOffer] [consumed_free_delivery] ${consumed_free_delivery}`);
      }

      if (discountEligible) {
        consumed_discount++;
        console.log(`[customeroffer] [getCustomerOffer] [note] discount eligible`);
        console.log(`[customeroffer] [getCustomerOffer] [consumed_discount] ${consumed_discount}`);
      }
      strapi.controllers.customeroffer.update({
        params: {
          id: customeroffer[0].id
        },
        request: {
          body: {
            consumed_free_delivery: consumed_free_delivery,
            consumed_discount: consumed_discount
          }
        }
      })
      return {
        applies: true,
        free_delivery: freeDeliveryEligible,
        discount: discountEligible,
        discount_amount: customeroffer[0].discount_amount
      }
    }
  }
};
