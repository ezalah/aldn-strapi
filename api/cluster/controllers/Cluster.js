'use strict';

/**
 * Cluster.js controller
 *
 * @description: A set of functions called "actions" for managing `Cluster`.
 */

module.exports = {

  /**
   * Retrieve cluster records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if(ctx.set) ctx.set('Content-Range', await Cluster.count());
    if (ctx.query._q) {
      return strapi.services.cluster.search(ctx.query);
    } else {
      return strapi.services.cluster.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a cluster record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.cluster.fetch(ctx.params);
  },

  /**
   * Count cluster records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.cluster.count(ctx.query, populate);
  },

  /**
   * Create a/an cluster record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.cluster.add(ctx.request.body);
  },

  /**
   * Update a/an cluster record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.cluster.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an cluster record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.cluster.remove(ctx.params);
  }
};
