'use strict';

/**
 * Lifecycle callbacks for the `Customerticket` model.
 */

const zendesk = require('node-zendesk');

const getModel = async (model, id) => {
  let ret = await model.findOne({
    params: {
      id: id
    }
  });
  return (ret || {
    attributes: {
      name: "undefined"
    }
  }).attributes;
}


module.exports = {
  // Before saving a value.
  // Fired before an `insert` or `update` query.
  // beforeSave: async (model, attrs, options) => {},

  // After saving a value.
  // Fired after an `insert` or `update` query.
  // afterSave: async (model, response, options) => {},

  // Before fetching a value.
  // Fired before a `fetch` operation.
  // beforeFetch: async (model, columns, options) => {},

  // After fetching a value.
  // Fired after a `fetch` operation.
  // afterFetch: async (model, response, options) => {},

  // Before fetching all values.
  // Fired before a `fetchAll` operation.
  // beforeFetchAll: async (model, columns, options) => {},

  // After fetching all values.
  // Fired after a `fetchAll` operation.
  // afterFetchAll: async (model, response, options) => {},

  // Before creating a value.
  // Fired before an `insert` query.
  // beforeCreate: async (model, attrs, options) => {},

  // After creating a value.
  // Fired after an `insert` query.
  afterCreate: async (model, attrs, options) => {
    console.log(`[customerticket] [afterCreate] lifecycle callback called`);

    console.log(`[customerticket] [afterCreate] [model] ${JSON.stringify(model)}`);
    const client = zendesk.createClient({
      username: 'webservices@aladdin.world',
      token: 'DlWyVgVs3z2QQPuVkjxRTKWnosj77ye6TN0sj62o',
      remoteUri: 'https://aladdinsupport.zendesk.com/api/v2'
    });

    const customer = await getModel(strapi.controllers.customer, model.attributes.customer_id);
    const customerhelp = await getModel(strapi.controllers.customerhelpoption, model.attributes.category_code);
    const customerhelpchild = await getModel(strapi.controllers.customerhelpoptionchild, model.attributes.problem_code);
    console.log(`[customerticket] [afterCreate] [customer] ${JSON.stringify(customer)}`);
    console.log(`[customerticket] [afterCreate] [category] ${JSON.stringify(customerhelp)}`);
    console.log(`[customerticket] [afterCreate] [problem] ${JSON.stringify(customerhelpchild)}`);

    let ticket = {
      "ticket": {
        "subject": model.attributes.subject,
        "requester": {
          "locale_id": 1,
          "name": customer.name,
          "email": `${customer.id}-${customer.country_code}${customer.phone_number}@customer.aladdin.email`,
          "organization": {
            "name": "Aladdin Consumers"
          }
          //"organization": {"name": "Aladdin Vendors"}
        },
        "comment": {
          "body": `${customerhelp.name}\n${customerhelpchild.name}\n${model.attributes.message}`
        }
      }
    };
    if (model.attributes.order_id)
      ticket.ticket.comment.body = `${ticket.ticket.comment.body} - https://jasminetest.4aldn.com/#/orders/${model.attributes.order_id}/show`

    console.log(`[customerticket] [afterCreate] [ticket] ${JSON.stringify(ticket)}`);
    client.tickets.create(ticket, function (err, req, result) {
      if (err) {
        strapi.log.error(err);
        return;
      };

      strapi.log.info(result);
    });
  },

  // Before updating a value.
  // Fired before an `update` query.
  // beforeUpdate: async (model, attrs, options) => {},

  // After updating a value.
  // Fired after an `update` query.
  // afterUpdate: async (model, attrs, options) => {},

  // Before destroying a value.
  // Fired before a `delete` query.
  // beforeDestroy: async (model, attrs, options) => {},

  // After destroying a value.
  // Fired after a `delete` query.
  // afterDestroy: async (model, attrs, options) => {}
};
