'use strict';

/**
 * Customerticket.js controller
 *
 * @description: A set of functions called "actions" for managing `Customerticket`.
 */

module.exports = {

  /**
   * Retrieve customerticket records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.customerticket.search(ctx.query);
    } else {
      return strapi.services.customerticket.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a customerticket record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.customerticket.fetch(ctx.params);
  },

  /**
   * Count customerticket records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.customerticket.count(ctx.query, populate);
  },

  /**
   * Create a/an customerticket record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.customerticket.add(ctx.request.body);
  },

  /**
   * Update a/an customerticket record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.customerticket.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an customerticket record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.customerticket.remove(ctx.params);
  }
};
