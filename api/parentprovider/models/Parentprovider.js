'use strict';

/**
 * Lifecycle callbacks for the `Parentprovider` model.
 */

const rand = require('randomatic');
const querystring = require('querystring');
const request = require('async-request');

module.exports = {
  // Before saving a value.
  // Fired before an `insert` or `update` query.
  // beforeSave: async (model, attrs, options) => {},

  // After saving a value.
  // Fired after an `insert` or `update` query.
  // afterSave: async (model, response, options) => {},

  // Before fetching a value.
  // Fired before a `fetch` operation.
  // beforeFetch: async (model, columns, options) => {},

  // After fetching a value.
  // Fired after a `fetch` operation.
  // afterFetch: async (model, response, options) => {},

  // Before fetching all values.
  // Fired before a `fetchAll` operation.
  // beforeFetchAll: async (model, columns, options) => {},

  // After fetching all values.
  // Fired after a `fetchAll` operation.
  // afterFetchAll: async (model, response, options) => {},

  // Before creating a value.
  // Fired before an `insert` query.
  // beforeCreate: async (model, attrs, options) => {},

  // After creating a value.
  // Fired after an `insert` query.
  afterCreate: async (model, attrs, options) => {
    const env = strapi.config.currentEnvironment;
    const signin_code = rand('0A', 10);
    console.log('[afterCreate_ParentProvider]', '[signin_code]', signin_code);

    try {
      if (strapi.config.environment === 'development' || strapi.config.environment === 'staging') {
        strapi.controllers.parentprovider.update({
          params: {
            id: attrs[0]
          },
          request: {
            body: {
              signin_code: signin_code
            }
          }
        });
      } else {
        const res = await strapi.controllers.util.SMSMisr_send_SMS(env.SMSMISR_USERNAME, env.SMSMISR_PASSWORD, env.SMSMISR_LANGUAGE_EN, env.SMSMISR_SENDER_ALADDIN, model.attributes.phone_number, `Your-signin-code-is:${signin_code}`);
        console.log(`[parentprovider] [afterCreate] [SMSMisr_send_SMS] [res] ${JSON.stringify(res)}`);

        if (resp.statusCode === "1901") {

          // updating the entry in DB
          strapi.controllers.parentprovider.update({
            params: {
              id: attrs[0]
            },
            request: {
              body: {
                signin_code: signin_code
              }
            }
          });
        }
      }
    } catch (error) {
      console.error(error);
    }
  },

  // Before updating a value.
  // Fired before an `update` query.
  // beforeUpdate: async (model, attrs, options) => {},

  // After updating a value.
  // Fired after an `update` query.
  // afterUpdate: async (model, attrs, options) => {},

  // Before destroying a value.
  // Fired before a `delete` query.
  // beforeDestroy: async (model, attrs, options) => {},

  // After destroying a value.
  // Fired after a `delete` query.
  // afterDestroy: async (model, attrs, options) => {}
};
