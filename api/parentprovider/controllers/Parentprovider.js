'use strict';

/**
 * Parentprovider.js controller
 *
 * @description: A set of functions called "actions" for managing `Parentprovider`.
 */

module.exports = {

  /**
   * Retrieve parentprovider records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if(ctx.set) ctx.set('Content-Range', await Parentprovider.count());
    if (ctx.query._q) {
      return strapi.services.parentprovider.search(ctx.query);
    } else {
      return strapi.services.parentprovider.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a parentprovider record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.parentprovider.fetch(ctx.params);
  },

  /**
   * Count parentprovider records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.parentprovider.count(ctx.query, populate);
  },

  /**
   * Create a/an parentprovider record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.parentprovider.add(ctx.request.body);
  },

  /**
   * Update a/an parentprovider record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.parentprovider.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an parentprovider record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.parentprovider.remove(ctx.params);
  }
};
