'use strict';

/**
 * Providerstats.js controller
 *
 * @description: A set of functions called "actions" for managing `Providerstats`.
 */

module.exports = {

  /**
   * Retrieve providerstats records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if(ctx.set) ctx.set('Content-Range', await Providerstats.count());
    if (ctx.query._q) {
      return strapi.services.providerstats.search(ctx.query);
    } else {
      return strapi.services.providerstats.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a providerstats record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.providerstats.fetch(ctx.params);
  },

  /**
   * Count providerstats records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.providerstats.count(ctx.query, populate);
  },

  /**
   * Create a/an providerstats record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.providerstats.add(ctx.request.body);
  },

  /**
   * Update a/an providerstats record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.providerstats.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an providerstats record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.providerstats.remove(ctx.params);
  }
};
