'use strict';

/**
 * Keepaliverequest.js controller
 *
 * @description: A set of functions called "actions" for managing `Keepaliverequest`.
 */

module.exports = {

  /**
   * Retrieve keepaliverequest records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if(ctx.set) ctx.set('Content-Range', await Keepaliverequest.count());
    if (ctx.query._q) {
      return strapi.services.keepaliverequest.search(ctx.query);
    } else {
      return strapi.services.keepaliverequest.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a keepaliverequest record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.keepaliverequest.fetch(ctx.params);
  },

  /**
   * Count keepaliverequest records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.keepaliverequest.count(ctx.query, populate);
  },

  /**
   * Create a/an keepaliverequest record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.keepaliverequest.add(ctx.request.body);
  },

  /**
   * Update a/an keepaliverequest record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.keepaliverequest.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an keepaliverequest record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.keepaliverequest.remove(ctx.params);
  },

  keepAliveReq: async (ctx) => {
    let dev = ctx.query.dev_id;
    await strapi.controllers.keepaliverequest.create({
      request: {
        body: {
          dev_id: dev
        }
      }
    });
    let res = await strapi.controllers.deviceglobalconf.findOne({
      params: {
        id: 1
      }
    });
    delete res.attributes.created_at;
    delete res.attributes.updated_at;
    console.log('[keepAliveReq]', '[query]', JSON.stringify(ctx.query), '[response]', JSON.stringify(res.attributes));
    return res;
  },

  keepAliveReq_post: async (ctx) => {
    let dev = ctx.request.body.dev_id;
    let body = ctx.request.body;
    body = JSON.stringify(body);
    await strapi.controllers.keepaliverequest.create({
      request: {
        body: {
          dev_id: dev,
          body: body
        }
      }
    });
    let res = await strapi.controllers.deviceglobalconf.findOne({
      params: {
        id: 1
      }
    });
    delete res.attributes.created_at;
    delete res.attributes.updated_at;
    console.log('[keepAliveReq_post]', '[body]', JSON.stringify(ctx.request.body), '[response]', JSON.stringify(res.attributes));
    return res.attributes;
  }
};
