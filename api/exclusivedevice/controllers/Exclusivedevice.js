'use strict';

/**
 * Exclusivedevice.js controller
 *
 * @description: A set of functions called "actions" for managing `Exclusivedevice`.
 */

module.exports = {

  /**
   * Retrieve exclusivedevice records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if (ctx.set) ctx.set('Content-Range', await Exclusivedevice.count());
    if (ctx.query._q) {
      return strapi.services.exclusivedevice.search(ctx.query);
    } else {
      return strapi.services.exclusivedevice.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a exclusivedevice record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.exclusivedevice.fetch(ctx.params);
  },

  /**
   * Count exclusivedevice records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.exclusivedevice.count(ctx.query, populate);
  },

  /**
   * Create a/an exclusivedevice record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.exclusivedevice.add(ctx.request.body);
  },

  /**
   * Update a/an exclusivedevice record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.exclusivedevice.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an exclusivedevice record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.exclusivedevice.remove(ctx.params);
  },

  registerExclusiveDevice: async (ctx) => {
    let dev = await strapi.controllers.device.find({
      query: {
        qr: ctx.request.body.device_qr
      }
    });

    if (dev.length === 0) {
      ctx.response.status = strapi.config.currentEnvironment.PROVIDER_FAILEDOP_CODE;
      return {
        message: "This qr code is not associated with any device in the system",
        status: strapi.config.currentEnvironment.PROVIDER_FAILEDOP_CODE
      };
    } else {
      let ret = await strapi.controllers.exclusivedevice.create(ctx);
      ret.attributes.status = 200;
      return ret;
    }
  }
};
