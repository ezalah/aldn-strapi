'use strict';

/**
 * Lifecycle callbacks for the `Address` model.
 */

const inside = require('point-in-polygon');

const getClusters = async (lng, lat) => {
  let clusters = await strapi.controllers.cluster.find({
    query: {}
  });

  let ret = [];
  // console.log(clusters);
  for (let cluster of clusters) {
    // console.log(cluster.boundaries);
    // console.log(JSON.parse(cluster.boundaries));
    // console.log([lng, lat]);
    // console.log(inside([lng, lat], JSON.parse(cluster.boundaries)));
    if (cluster.boundaries && inside([lng, lat], JSON.parse(cluster.boundaries))) ret.push(cluster.id);
  }
  return ret;
}

const assignToCluster = async (model, attrs) => {
  // console.log(model.attributes);
  let clusters = await getClusters(model.attributes.lng, model.attributes.lat);
  // console.log(clusters);
  if (clusters.length !== 0) {
    strapi.controllers.address.update({
      params: {
        id: attrs[0]
      },
      request: {
        body: {
          cluster: clusters[0]
        }
      }
    })
  } else console.error('No cluster available for customer with id', attrs[0]);
}

module.exports = {
  // Before saving a value.
  // Fired before an `insert` or `update` query.
  // beforeSave: async (model, attrs, options) => {},

  // After saving a value.
  // Fired after an `insert` or `update` query.
  // afterSave: async (model, response, options) => {},

  // Before fetching a value.
  // Fired before a `fetch` operation.
  // beforeFetch: async (model, columns, options) => {},

  // After fetching a value.
  // Fired after a `fetch` operation.
  // afterFetch: async (model, response, options) => {},

  // Before fetching all values.
  // Fired before a `fetchAll` operation.
  // beforeFetchAll: async (model, columns, options) => {},

  // After fetching all values.
  // Fired after a `fetchAll` operation.
  // afterFetchAll: async (model, response, options) => {},

  // Before creating a value.
  // Fired before an `insert` query.
  // beforeCreate: async (model, attrs, options) => {},

  // After creating a value.
  // Fired after an `insert` query.
  afterCreate: async (model, attrs, options) => {
    // console.log(model);
    assignToCluster(model, attrs);
  },

  // Before updating a value.
  // Fired before an `update` query.
  // beforeUpdate: async (model, attrs, options) => {},

  // After updating a value.
  // Fired after an `update` query.
  // afterUpdate: async (model, attrs, options) => {
  //   assignToCluster(model, attrs);
  // },

  // Before destroying a value.
  // Fired before a `delete` query.
  // beforeDestroy: async (model, attrs, options) => {},

  // After destroying a value.
  // Fired after a `delete` query.
  // afterDestroy: async (model, attrs, options) => {}
};
