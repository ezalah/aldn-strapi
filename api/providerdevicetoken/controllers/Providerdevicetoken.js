'use strict';

/**
 * Providerdevicetoken.js controller
 *
 * @description: A set of functions called "actions" for managing `Providerdevicetoken`.
 */

module.exports = {

  /**
   * Retrieve providerdevicetoken records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.providerdevicetoken.search(ctx.query);
    } else {
      return strapi.services.providerdevicetoken.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a providerdevicetoken record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.providerdevicetoken.fetch(ctx.params);
  },

  /**
   * Count providerdevicetoken records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.providerdevicetoken.count(ctx.query, populate);
  },

  /**
   * Create a/an providerdevicetoken record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.providerdevicetoken.add(ctx.request.body);
  },

  /**
   * Update a/an providerdevicetoken record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.providerdevicetoken.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an providerdevicetoken record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.providerdevicetoken.remove(ctx.params);
  }
};
