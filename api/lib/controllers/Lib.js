'use strict';

/**
 * Lib.js controller
 *
 * @description: A set of functions called "actions" for managing `Lib`.
 */

const inside = require('point-in-polygon');

module.exports = {

  /**
   * Retrieve lib records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if (ctx.set) ctx.set('Content-Range', await Lib.count());
    if (ctx.query._q) {
      return strapi.services.lib.search(ctx.query);
    } else {
      return strapi.services.lib.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a lib record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.lib.fetch(ctx.params);
  },

  /**
   * Count lib records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.lib.count(ctx.query, populate);
  },

  /**
   * Create a/an lib record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.lib.add(ctx.request.body);
  },

  /**
   * Update a/an lib record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.lib.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an lib record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.lib.remove(ctx.params);
  },

  getCluster: async (ctx) => {
    let lng = ctx.request.body.lng;
    let lat = ctx.request.body.lat;
    let clusters = await strapi.controllers.cluster.find(ctx);
    for (let cluster of clusters) {
      //   if (inside([lng, lat], JSON.parse(cluster.boundaries)) {
      if (inside([lng, lat], JSON.parse(cluster.boundaries))) return cluster;
      //     }
    }
    return {
      message: "Point doesn't belong to any registered cluster"
    };
  },

  calculatePrice: async (original_price, delivery_fees, freeDeliveryEligible, discountEligible, discount_amount) => {
    return Math.max(0, original_price - freeDeliveryEligible * delivery_fees - discountEligible * discount_amount);
  },

  calculatePriceTester: async (ctx) => {
    return strapi.controllers.lib.calculatePrice(ctx.request.body.original_price, ctx.request.body.delivery_fees, ctx.request.body.freeDeliveryEligible, ctx.request.body.discountEligible, ctx.request.body.discount_amount);
  }
};
