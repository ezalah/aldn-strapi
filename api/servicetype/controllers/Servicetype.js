'use strict';

/**
 * Servicetype.js controller
 *
 * @description: A set of functions called "actions" for managing `Servicetype`.
 */

module.exports = {

  /**
   * Retrieve servicetype records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, {
    populate
  } = {}) => {
    if(ctx.set) ctx.set('Content-Range', await Servicetype.count());
    if (ctx.query._q) {
      return strapi.services.servicetype.search(ctx.query);
    } else {
      return strapi.services.servicetype.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a servicetype record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.servicetype.fetch(ctx.params);
  },

  /**
   * Count servicetype records.
   *
   * @return {Number}
   */

  count: async (ctx, next, {
    populate
  } = {}) => {
    return strapi.services.servicetype.count(ctx.query, populate);
  },

  /**
   * Create a/an servicetype record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.servicetype.add(ctx.request.body);
  },

  /**
   * Update a/an servicetype record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.servicetype.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an servicetype record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.servicetype.remove(ctx.params);
  }
};
