const functions = {
  createdAtTime: async (update, id) => {
    update({
      params: {
        id: id
      },
      request: {
        body: {
          createdAt: new Date().toISOString().slice(0, 19).replace('T', ' ')

        }
      }
    });
  },

  updatedAtTime: async (update, id) => {
    console.log('hello');
    update({
      params: {
        id: id
      },
      request: {
        body: {
          updatedAt: new Date().toISOString().slice(0, 19).replace('T', ' ')

        }
      }
    })
  }
};

module.exports = functions;
