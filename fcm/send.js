const fcm = require('fcm-notification');
const FCM = new fcm(`${__dirname}/aladdin-consumer-app-firebase-adminsdk.json`);

class sendFCM {
  static sendNotification(tokens, message) {
    console.log(`[fcm] [send] [tokens] ${JSON.stringify(tokens)}`);
    console.log(`[fcm] [send] [message] ${JSON.stringify(message)}`);
    FCM.sendToMultipleToken(message, tokens, (err, res) => {
      if (err) {
        console.log(`[fcm] [send] [sendNotification] [sendToMultipleTokens] [err] ${JSON.stringify(err)}`);
      } else {
        console.log(`[fcm] [send] [sendNotification] [sendToMultipleTokens] [res] ${JSON.stringify(res)}`);
      }
    });
  }
}

module.exports = sendFCM;
